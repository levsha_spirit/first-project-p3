import os

TEMPLATE_FOLDER = os.path.abspath("templates")
STATIC_FOLDER = os.path.abspath("static")
DATABASE_FILE = os.path.abspath("database.sqlite3")