import argparse
import sqlite3
import database.initialization
import database.connection


commands = {}


def command(name):
    def wrapper(callback):
        commands[name] = callback
        return callback
    return wrapper


@command("initdb")
def init_db():
    print(f"Init DB using schema {database.initialization.SQL_SCHEMA}")
    database.initialization.init_db()
    print("OK.")


@command("sql-console")
def sql_console():
    try:
        while True:
            query = input("SQL:\ ")
            if query == "!exit":
                break

            try:
                with  database.connection.connect() as db:
                     print(db.execute(query).fetchall())
            except(sqlite3.ProgrammingError, sqlite3.OperationalError) as e:
                print(e)
    except (KeyboardInterrupt, EOFError):
        print("Ctrl+C/Ctrl+Z")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("command", choices=commands.keys())
    args = parser.parse_args()
    commands[args.command]()

