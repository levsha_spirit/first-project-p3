import flask

import config
import views

app = flask.Flask(
    __name__,
    template_folder=config.TEMPLATE_FOLDER,
    static_folder=config.STATIC_FOLDER
)
app.register_blueprint(views.views)





