import sqlite3
import database.connection as db_con


SQL_SCHEMA = """
PRAGMA foreign_keys = ON;

DROP TABLE IF EXISTS subjects;
DROP TABLE IF EXISTS comments;


CREATE TABLE subjects (
    id INTEGER PRIMARY KEY,
    title TEXT NOT NULL,
    description TEXT NOT NULL
);

CREATE TABLE comments (
    id INTEGER PRIMARY KEY,
    title TEXT NOT NULL,
    text TEXT NOT NULL,
    timestamp FLOAT NOT NULL,

    subject_id INTEGER NOT NULL,
    FOREIGN KEY (subject_id) REFERENCES subject(id)
);
"""


def init_db():
    with db_con.connect() as db:
        db.executescript(SQL_SCHEMA)
