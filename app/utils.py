import datetime


def get_server_time():
    return datetime.datetime.utcnow()


def format_time(timestamp: float) -> str:
    return(
        datetime.datetime.fromtimestamp(timestamp) + datetime.timedelta(hours=4)

    ).strftime("%Y.%m.%d, %H:%M:%S")
