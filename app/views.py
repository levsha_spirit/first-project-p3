import flask

import database.utils as db_utils
import utils


views = flask.Blueprint("views", __name__)


@views.route("/subjects/<int:subject_id>", methods=["GET", "POST"])
def view_subject(subject_id):
    try:
        subject = db_utils.get_subject(subject_id)
    except ValueError as e:
        flask.abort(404, *e.args)

    if flask.request.method == "POST":
        title = flask.request.form["title"]
        text = flask.request.form["text"]
        db_utils.add_comment(subject_id, title, text)

    return flask.render_template(
        "subject.html",
        subject=subject,
        comments=db_utils.get_comments(subject_id),
        format_time=utils.format_time,
    )


@views.route("/")
def view_index():
    return flask.render_template("index.html", subjects=db_utils.get_subjects())


@views.route("/subjects/<int:subject_id>/delete/all")
def delete_all(subject_id):
    db_utils.delete_comments(subject_id)
    return flask.redirect("/")
